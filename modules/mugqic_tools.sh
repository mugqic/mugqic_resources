#!/bin/bash
# Exit immediately on error
set -eu -o pipefail

#
# MUGQIC Tools
#

SOFTWARE=mugqic_tools
VERSION=1.10.4

# 'MUGQIC_INSTALL_HOME_DEV' for development, 'MUGQIC_INSTALL_HOME' for production (don't write '$' before!)
INSTALL_HOME=MUGQIC_INSTALL_HOME

# Indirection call to use $INSTALL_HOME value as variable name
INSTALL_DIR=${!INSTALL_HOME}/software/$SOFTWARE

# Create install directory with permissions if necessary
if [[ ! -d $INSTALL_DIR ]]
then
  mkdir $INSTALL_DIR
  chmod ug+rwX,o+rX-w $INSTALL_DIR
fi

INSTALL_DOWNLOAD=$INSTALL_DIR/tmp
mkdir -p $INSTALL_DOWNLOAD
cd $INSTALL_DOWNLOAD

# Download, extract, build
# Write here the specific commands to download, extract, build the software, typically similar to:
ARCHIVE=$SOFTWARE-$VERSION.tar.gz
# If archive was previously downloaded, use the local one, otherwise get it from remote site
if [[ -f ${!INSTALL_HOME}/archive/$ARCHIVE ]]
then
  echo "Archive $ARCHIVE already in ${!INSTALL_HOME}/archive/: using it..."
  cp -a ${!INSTALL_HOME}/archive/$ARCHIVE .
else
  echo "Archive $ARCHIVE not in ${!INSTALL_HOME}/archive/: downloading it..."
  wget https://bitbucket.org/mugqic/$SOFTWARE/get/$VERSION.tar.gz
  # Rename archive
  mv $VERSION.tar.gz $ARCHIVE
fi
tar zxvf $ARCHIVE

SOFTWARE_DIR=$SOFTWARE-$VERSION
# Rename mugqic_tools directory since original bitbucket name contains the commit number instead of version
mv mugqic-mugqic_tools* $SOFTWARE_DIR

# Add permissions and install software
cd $INSTALL_DOWNLOAD
chmod -R ug+rwX,o+rX-w .
mv -i $SOFTWARE_DIR $INSTALL_DIR/
# Store archive if not already present or if different from the previous one
if [[ ! -f ${!INSTALL_HOME}/archive/$ARCHIVE || `diff ${!INSTALL_HOME}/archive/$ARCHIVE $ARCHIVE` ]]
then
  mv -i $ARCHIVE ${!INSTALL_HOME}/archive/
fi

# Module file
echo "#%Module1.0
proc ModulesHelp { } {
       puts stderr \"\tMUGQIC - $SOFTWARE \" ;
}
module-whatis \"$SOFTWARE  \" ;
                      
set             root                \$::env($INSTALL_HOME)/software/$SOFTWARE/$SOFTWARE_DIR ;
prepend-path    PATH                \$root/tools ;
prepend-path    PATH                \$root/perl-tools ;
prepend-path    PATH                \$root/R-tools ;
prepend-path    PATH                \$root/python-tools ;
prepend-path    PATH                \$root/RRNATagger-tools ;
prepend-path    PERL5LIB            \$root/perl-tools ;
setenv          R_TOOLS             \$root/R-tools ;
setenv          PERL_TOOLS          \$root/perl-tools ;
setenv          PYTHON_TOOLS        \$root/python-tools ;
setenv          JAVA_TOOLS          \$root/java-tools ;
" > $VERSION

################################################################################
# Everything below this line should be generic and not modified

# Well... here, module directory is named "tools" instead of "mugqic_tools" for aesthetical reasons

# Default module version file
echo "#%Module1.0
set ModulesVersion \"$VERSION\"" > .version

# Set module directory path by lowercasing $INSTALL_HOME and removing '_install_home' in it
MODULE_DIR=${!INSTALL_HOME}/modulefiles/`echo ${INSTALL_HOME,,} | sed 's/_install_home//'`/tools

# Create module directory with permissions if necessary
if [[ ! -d $MODULE_DIR ]]
then
  mkdir $MODULE_DIR
  chmod ug+rwX,o+rX-w $MODULE_DIR
fi

# Add permissions and install module
chmod ug+rwX,o+rX-w $VERSION .version
mv $VERSION .version $MODULE_DIR/

# Clean up temporary installation files if any
cd
rm -rf $INSTALL_DOWNLOAD
